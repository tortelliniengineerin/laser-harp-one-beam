/**
 * @file scales.c
 * @ingroup Laserharp
 * @author Nick Felker
 * @code
 */

/*
 * scales.c
 *
 *  Created on: Apr 29, 2016
 *      Author: Nick Felker
 * 		Frequency Manipulator: Mike Paule
 */

#include "scales.h"
#include "system.h"

//Let us define some built-in scales
static scale C_Major = {7, {_NOTE_C, _NOTE_D, _NOTE_E, _NOTE_F, _NOTE_G, _NOTE_A, _NOTE_B}};
static scale G_Major = {7, {_NOTE_G, _NOTE_A, _NOTE_B, _NOTE_C, _NOTE_D, _NOTE_E, _NOTE_Fs}};
static scale D_Major = {7, {_NOTE_D, _NOTE_E, _NOTE_Fs, _NOTE_G, _NOTE_A, _NOTE_B, _NOTE_Cs}};
static scale A_Major = {7, {_NOTE_A, _NOTE_B, _NOTE_Cs, _NOTE_D, _NOTE_E, _NOTE_Fs, _NOTE_Gs}};
static scale E_Major = {7, {_NOTE_E, _NOTE_Fs, _NOTE_Gs, _NOTE_A, _NOTE_B, _NOTE_Cs, _NOTE_Ds}};
static scale B_Major = {7, {_NOTE_B, _NOTE_Cs, _NOTE_Ds, _NOTE_E, _NOTE_Fs, _NOTE_Gs, _NOTE_As}};
static scale Gb_Major = {7, {_NOTE_Gb, _NOTE_Ab, _NOTE_Bb, _NOTE_B, _NOTE_Db, _NOTE_Eb, _NOTE_F}};
static scale Db_Major = {7, {_NOTE_Db, _NOTE_Eb, _NOTE_F, _NOTE_Gb, _NOTE_Ab, _NOTE_Bb, _NOTE_C}};
static scale Ab_Major = {7, {_NOTE_Ab, _NOTE_Bb, _NOTE_C, _NOTE_Db, _NOTE_Eb, _NOTE_F, _NOTE_G}};
static scale Eb_Major = {7, {_NOTE_Eb, _NOTE_F, _NOTE_G, _NOTE_Ab, _NOTE_Bb, _NOTE_C, _NOTE_D}};
static scale Bb_Major = {7, {_NOTE_Bb, _NOTE_C, _NOTE_D, _NOTE_Eb, _NOTE_F, _NOTE_G, _NOTE_A}};
static scale F_Major = {7, {_NOTE_F, _NOTE_G, _NOTE_A, _NOTE_Bb, _NOTE_C, _NOTE_D, _NOTE_E}};
static scale Blues = {6, {_NOTE_C, _NOTE_Eb, _NOTE_F, _NOTE_Fs, _NOTE_G, _NOTE_Bb}};
static scale Bebop = {8, {_NOTE_C, _NOTE_D, _NOTE_E, _NOTE_F, _NOTE_G, _NOTE_A, _NOTE_Bb, _NOTE_B}};
static scale Hirajoshi = {5, .notes = {_NOTE_C, _NOTE_D, _NOTE_Eb, _NOTE_G, _NOTE_Ab}};
static scale Yo = {5, {_NOTE_C, _NOTE_Eb, _NOTE_F, _NOTE_G, _NOTE_B}};
static scale SexyChord = {3, {_NOTE_C, _NOTE_Eb, _NOTE_A}};

char getNoteAtOctave(char note, char octave) {
	return note*octave*12;
}

char* getScaleAtOctave(scale thisScale, char octave) {
	char newScale[MAX_BEAMS];
	unsigned char index = 0;
	unsigned char sized_index = 0;
	unsigned char newScaleLength = (thisScale.scale_size > MAX_BEAMS)?MAX_BEAMS:thisScale.scale_size;

	while(sized_index < MAX_BEAMS) { //Cyclical, if our scale has fewer than the MAX_BEAMS size
		if(sized_index + newScaleLength > MAX_BEAMS)
			newScaleLength = MAX_BEAMS - sized_index; //If we're on the last few notes in the scale, take those remains as the size
		for(index=0;index<newScaleLength;index++) {
			if(sized_index > 0 && (sized_index+index)%thisScale.scale_size == 0) { //Move to the next octave when the first octave overflows
				octave++;
			}
			newScale[sized_index+index] = thisScale.notes[index]+octave*12; //Shift our note to the given octave
		}
		sized_index += index;
	}
	return &newScale;
}

scale getScale(char scale_name) {
	switch(scale_name) {
	case C_MAJOR:
		return C_Major;
	case G_MAJOR:
		return G_Major;
	case D_MAJOR:
		return D_Major;
	case A_MAJOR:
		return A_Major;
	case E_MAJOR:
		return E_Major;
	case B_MAJOR:
		return B_Major;
	case Gb_MAJOR:
		return Gb_Major;
	case Db_MAJOR:
		return Db_Major;
	case Ab_MAJOR:
		return Ab_Major;
	case Eb_MAJOR:
		return Eb_Major;
	case Bb_MAJOR:
		return Bb_Major;
	case F_MAJOR:
		return F_Major;
	case BLUES_SCALE:
		return Blues;
	case BEBOP_SCALE:
		return Bebop;
	case HIRAJOSHI_SCALE:
		return Hirajoshi;
	case YO_SCALE:
		return Yo;
	case SEXY_CHORD:
		return SexyChord;
	default:
		return C_Major;
	}
}
scale shiftScale(scale origScale, char offset) {
	scale newScale;
	newScale.scale_size = origScale.scale_size;
	unsigned int i = 0;
	for(i=0;i<origScale.scale_size;i++) {
		newScale.notes[i] = origScale.notes[i]+offset;
	}
	return newScale;
}

/** @endcode */
