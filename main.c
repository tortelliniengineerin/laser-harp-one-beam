/**
 * @file main.c
 * @ingroup Laserharp
 * @author Michael Paule
 * @author Chris Frederickson
 * @author Nick Felker
 * @author Kyle Siebens
 * @code
 */

// Christopher Frederickson, Michael Paule, Nick Felker, Kyle Siebens
// Rowan University Embedded Software
// February-May 2016

#include <msp430.h>
#include <stdint.h>
#include "system.h"

#define MIDI_MODE_UART 0
#define MIDI_MODE_BLE  1
#define USE_MIDI 0

void setVcoreUp (unsigned int level);
void setClockSpeed(void);
void buttonPress(void);

//clock speed functions
void setVcoreUp (unsigned int level){
  // Open PMM registers for write
  PMMCTL0_H = PMMPW_H;
  // Set SVS/SVM high side new level
  SVSMHCTL = SVSHE + SVSHRVL0 * level + SVMHE + SVSMHRRL0 * level;
  // Set SVM low side to new level
  SVSMLCTL = SVSLE + SVMLE + SVSMLRRL0 * level;
  // Wait till SVM is settled
  while ((PMMIFG & SVSMLDLYIFG) == 0);
  // Clear already set flags
  PMMIFG &= ~(SVMLVLRIFG + SVMLIFG);
  // Set VCore to new level
  PMMCTL0_L = PMMCOREV0 * level;
  // Wait till new level reached
  if ((PMMIFG & SVMLIFG))
    while ((PMMIFG & SVMLVLRIFG) == 0);
  // Set SVS/SVM low side to new level
  SVSMLCTL = SVSLE + SVSLRVL0 * level + SVMLE + SVSMLRRL0 * level;
  // Lock PMM registers for write access
  PMMCTL0_H = 0x00;
}

void setClockSpeed(void){
	//@todo Clean up comments.
    //increase the clock speed to 25MHz
	//clock configuration- Mulby's style, but changed to 20Mhz because that is what my User guide has values for the UART for
    //this clock is to make the 115200 Baud work well... i think. This code works (i believe) and

    //...
    //the following would typically be located at the top of main
    // Increase Vcore setting to level3 to support fsystem=25MHz ///// not sure if I need to change that for 20MHz
    // NOTE: Change core voltage one level at a time..
	setVcoreUp (0x01);
	setVcoreUp (0x02);
	setVcoreUp (0x03);
    UCSCTL3 = SELREF_2;                       // Set DCO FLL reference = REFO
    UCSCTL4 |= SELA_2;                        // Set ACLK = REFO
    __bis_SR_register(SCG0);                  // Disable the FLL control loop
    UCSCTL0 = 0x0000;                         // Set lowest possible DCOx, MODx
    UCSCTL1 = DCORSEL_7;                      // Select DCO range 50MHz operation
    UCSCTL2 = FLLD_0 + 759;                   // Set DCO Multiplier for 20MHz
                                            // (N + 1) * FLLRef = Fdco
                                            // (759 + 1) * 32768 = 25MHz
                                            // Set FLL Div = fDCOCLK/2
    __bic_SR_register(SCG0);                  // Enable the FLL control loop
    // Worst-case settling time for the DCO when the DCO range bits have been
    // changed is n x 32 x 32 x f_MCLK / f_FLL_reference. See UCS chapter in 5xx
    // UG for optimization.
    // 32 x 32 x 25 MHz / 32,768 Hz ~ 780k MCLK cycles for DCO to settle
    __delay_cycles(782000);
    // Loop until XT1,XT2 & DCO stabilizes - In this case only DCO has to stabilize
    do
    {
    UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + DCOFFG);
                                            // Clear XT2,XT1,DCO fault flags
    SFRIFG1 &= ~OFIFG;                      // Clear fault flags
    }while (SFRIFG1&OFIFG);                   // Test oscillator fault flag
}

void buttonPress(void){
	static char held = 0;
	if(!(P2IN&BIT1) && !held){
		laserHarpStartStop();
		held = 1;
	} else {
		held = 0;
	}
}

void sendMidiOverUart(uint8_t * ptr, uint8_t len) {
	UART_Write(1, ptr, len);
}

 int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	       // Stop watchdog timer
    setClockSpeed();

    DisableInterrupts();

    //Embedded Library Init
    Timing_Init();
    Task_Init();
    UART_Init(UART_CHANNEL);

#if USE_MIDI == MIDI_MODE_BLE
    //BLE Midi Init
    MIDI_BLE_Init();
#elif USE_MIDI == MIDI_MODE_UART
    MIDI_Init(sendMidiOverUart);
#endif

    //Motor Driver Init
    hal_MotorDriver_Init();
    MotorEncoder_Init();

    //Laser Harp Specific Init
    laserHarpInit();

    EnableInterrupts();

    // Button Init
    P2REN|=BIT1; //Enable resistor on P2.1
    P2OUT|=BIT1; //Set resistor to pull-up
    Task_Schedule(buttonPress,0,100,100);

    UART_Printf(UART_CHANNEL, "Init finished!\r\n");

    while(1){
    	SystemTick();
    }
	return 0;
}

 /** @endcode */
