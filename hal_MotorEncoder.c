/**
 * @file hal_MotorEncoder.c
 * @ingroup hal_Laserharp
 * @author Michael Paule
 * @author Kyle Siebens
 * @code
 */
#include "system.h"
/*
 * hal_MotorEncoder.c
 *
 *  Created on: Apr 4, 2016
 *      Author: Michael Paule
 *
 *  This 48 CPR quadrature encoder (so 12 magnets, 2 hall effects, counts on pos and neg edges, = 48) uses a Hall effect sensor, so it is not as easy to get exact position as an optical encoder with grey code in it
 *	This HAL looks for 2 positive edges to begin, then flips its edge sensativity to count alternating positive and negative edges of the encoder
 *
 */


void hal_MotorEncoder_Init(void){
	// Init digital interrupt //P1.5 is input by default @force it to be input P1DIR &= ~(BIT5)
	P1DIR &= ~(BIT4);
	P1DIR &= ~(BIT5);

	P1IE |= BIT4; // Interrupt on Input Pin P1.4 (channel A)
	P1IES &= ~BIT4; //  Low to High Edge detect

	P1IE |= BIT5; // Interrupt on Input Pin P1.5 (channel B)
	P1IES &= ~BIT5; //  Low to High Edge detect
}


#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT1_VECTOR
__interrupt void PORT1_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(PORT1_VECTOR))) PORT1_ISR (void)
#else
#error Compiler not supported!
#endif
{
	//EncoderProcessEdge();	//process the edge in MotorEncoder.c

	switch(__even_in_range(P1IV,14))
	{
	case  0: break;                          //
	case  2: break;                          //P1.0
	case  4: break;                          //P1.1
	case  6: break;                          //P1.2
	case  8: break;                          //P1.3
	case 10: EncoderProcessEdge();	//process the edge in MotorEncoder.c
		     P1IES ^= BIT4;  				 //P1.4 interrupt- flip edge detection
			 P1IFG &= ~BIT4; 				 //Clear Interrupt Flag
		break;                          	 //
	case 12: EncoderProcessEdge();	//process the edge in MotorEncoder.c
			 P1IES ^= BIT5;					 // P1.5 interrupt- flip edge detection
			 P1IFG &= ~BIT5; 				 // Clear Interrupt Flag
		break;
	case 14: break;
	default: break;
	}

}

/** @endcode */


