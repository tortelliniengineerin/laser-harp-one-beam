#ifndef HAL_MOTORDRIVER_H_
#define HAL_MOTORDRIVER_H_
/**
 * @author Michael Paule
 *
 * This module contains the 2 Hardware Abstraction Layers for the laser harp, the hal_MotorDrive and the hal_MotorEncoder.
 *
 *
 *
 * @defgroup hal_Laserharp HAL Module
 * @file hal_MotorDriver.h
 * @ingroup hal_Laserharp
 *
 * @author Michael Paule
 *
  * hal_MotorDriver.h
 *
 *  Created on: Apr 2, 2016
 *  Author: Michael Paule
 *
 *	This is a driver for TB6612FNG Dual Motor Driver - only using channel A to drive a single motor (for the laser harp)
 *	This motor driver has the option to PWM the motor to slow its speed down, but current build will be trying full speed
 *
 *  PWMA - P6.6
 *  AIN2 - P6.1
 *  AIN1 - P6.2
 *  STBY - P6.3
 * @{
 */

/*
 * hal_MotorDriver.h
 *
 *  Created on: Apr 2, 2016
 *  Author: Michael Paule
 *
 *	This is a driver for TB6612FNG Dual Motor Driver - only using channel A to drive a single motor (for the laser harp)
 *	This motor driver has the option to PWM the motor to slow its speed down, but current build will be trying full speed
 *
 *  PWMA - P6.6
 *  AIN2 - P6.1
 *  AIN1 - P6.2
 *  STBY - P6.3
 *
 */


#include "system.h"

#define PWMA 0x0040		//P6.6
#define AIN2 0x0002		//P6.1
#define AIN1 0x0004		//P6.2
#define STBY 0x0008		//P6.3 Standby must be driven high to get out of standby mode

/** @brief Initilizes pins and starts the system in standby state with standby pin high (standby mode)
 *
 *
 */
void hal_MotorDriver_Init(void);

/** @brief Switchs motor to SpinCW state
 *
 * Sends the Clock wise spin control function to the motor
 */
void hal_MotorDriver_SpinCW(void);

/** @brief Switchs motor to SpinCCW state
 *
 * Sends the Counter Clock wise spin control function
 *
 */
void hal_MotorDriver_SpinCCW(void);

/** @brief Switchs motor to the stopped state
 *
 * Sends the stop control function to the Motor Controller
 */
void hal_MotorDriver_Stop(void);

/** @brief Switchs motor to standby state
 *
 *  Low is standby (high impedence on output no matter the other control inputs), high is normal operation
 *   (the other function when called automatically take the motor out of standby)
 *
 */
void hal_MotorDriver_Standby(void);

/** @} */
#endif /* HAL_MOTORDRIVER_H_ */
