/**
 * @file MotorEncoder.c
 * @ingroup Laserharp
 * @author Michael Paule
 * @code
 */

/*
 * MotorEncoder.c
 *
 *  Created on: Apr 4, 2016
 *      Author: Michael Paule
 *
 *
 *  Count the time inbetween interupts, which is 1/12th of a revolution, to get the rpm
 *	Use the RPM to interpolate posistion between interupts so that the laser can fire at certain positions
 *	Everytime and interupt is triggered, the system should search for the closest fire angle/position and
 *	set timerA1 to wait for that amount of time. Then in the timer A 1 ISR, switch on and off the laser
 *	(note the task manager/timing module uses timerA0 so I am using timerA1 so not to step on any toes)
 */

#include "system.h"

static uint8_t encoderIndex;		// Keeps track of encoder position
static encoder_edge_callback_t edge_callback = 0;

void MotorEncoder_Init(void){
	hal_MotorEncoder_Init();
	encoderIndex = 0;
}

/*
 * Counts the number of edges
 */
void EncoderProcessEdge(void){
	if (encoderIndex < 47) { //so at 47 it gets set to 0
		encoderIndex++;
	} else {
		encoderIndex = 0;
	}
	if (edge_callback) (*edge_callback)(encoderIndex);
}

void RegisterEdgeCallback(encoder_edge_callback_t new_edge_callback){
	edge_callback = new_edge_callback;
}

/** @endcode */
