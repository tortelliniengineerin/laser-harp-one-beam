/**
 * @author Michael Paule
 *
 * @ingroup Laserharp
 * @file MotorEncoder.h
 *
 *  Created on: Apr 4, 2016
 *      Author: Michael Paule
 *
 *      Handles Processing the Encoder edges,
 *      In every Encoder Magnet interupt, the processor searches for ?????
 * @{
 */

/*
 * MotorEncoder.h
 *
 * Created on: Apr 4, 2016
 *   Author: Michael Paule
 *
 *   Handles Processing the Encoder edges,
 *   In every Encoder Magnet interupt, the processor searches for ?????
 */

#ifndef MOTORENCODER_H_
#define MOTORENCODER_H_

typedef void (*encoder_edge_callback_t)(uint8_t encoderIndex);

/**
 * @brief Initilizes the Hal layer
 *
 *
 */
void MotorEncoder_Init(void);

/**
 * @brief
 *
 * Called from HAL when a pos edge has been reciever
 * Increments a counter
 */
void EncoderProcessEdge(void);

/** @brief
 *
 * @param
 *
 */
void RegisterEdgeCallback(encoder_edge_callback_t edge_callback);

/** @brief
 *
 *
 */
void getSpeed(void);

/** @brief
 *
 * Returns the position of the last encoder passed. Which
 */
void getPosition(void);

/** @} */
#endif /* MOTORENCODER_H_ */
