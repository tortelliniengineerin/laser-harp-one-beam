/**
 * @file harp.c
 * @ingroup Laserharp
 * @author Michael Paule
 * @author Chris Frederickson
 * @author Nick Felker
 * @author Kyle Siebens
 * @code
 */


/*
 * harp.c
 *
 *  Created on: Apr 26, 2016
 *      Author: Chris and Mike
 */
#include "system.h"
#include "harp.h"
#include "scales.h"
#include "MotorEncoder.h"

uint8_t beam_broken_flag = 0;
static beam_t * current_beam;
static uint8_t harp_enable = 0;

static beam_t beam_array[MAX_BEAMS] = {
	{.is_valid = true, .position = 0, .note = 72, .average_intensity = 0, .note_on_flag = false, .beam_broken_flag = false, .lockout_flag = false},
	{.is_valid = true, .position = 0, .note = 73, .average_intensity = 0, .note_on_flag = false, .beam_broken_flag = false, .lockout_flag = false},
	{.is_valid = true, .position = 0, .note = 74, .average_intensity = 0, .note_on_flag = false, .beam_broken_flag = false, .lockout_flag = false},
	{.is_valid = true, .position = 0, .note = 75, .average_intensity = 0, .note_on_flag = false, .beam_broken_flag = false, .lockout_flag = false},
	{.is_valid = true, .position = 0, .note = 76, .average_intensity = 0, .note_on_flag = false, .beam_broken_flag = false, .lockout_flag = false},
	{.is_valid = true, .position = 0, .note = 77, .average_intensity = 0, .note_on_flag = false, .beam_broken_flag = false, .lockout_flag = false},
	{.is_valid = true, .position = 0, .note = 78, .average_intensity = 0, .note_on_flag = false, .beam_broken_flag = false, .lockout_flag = false},
	{.is_valid = true, .position = 0, .note = 79, .average_intensity = 0, .note_on_flag = false, .beam_broken_flag = false, .lockout_flag = false},
	{.is_valid = true, .position = 0, .note = 80, .average_intensity = 0, .note_on_flag = false, .beam_broken_flag = false, .lockout_flag = false}
};

void laserHarpInit(void){
	// ADC Init
	ADC12CTL0 = ADC12SHT02 + ADC12ON;         // Sampling time, ADC12 on
	ADC12CTL1 = ADC12SHP;                     // Use sampling timer
	ADC12IE = 0x01;                           // Enable interrupt
	ADC12CTL0 |= ADC12ENC;
	P6SEL |= 0x01;                            // P6.0 ADC option select

	P4DIR |= BIT3;					// Laser Control Output (Pin 4.3)

	TA1CTL |= TASSEL_2;				// Sets the clock used for Timer1_A to SMCLK and halts the timer to start
	TA1CCTL0 |= CCIE; 				//enable the interupts only on TA1CCR0 - I believe before the interupts kept firing because at 0 TA1CCR1,2,3 etc. kept firing

	TA1CCR0 = PULSE_LENGTH*(FCPU/1000000);	// the number of ticks in PULSE_LENGTH micro seconds
	RegisterEdgeCallback(&LaserFireCheck);
	laserHarpSetScale(getScale(C_MAJOR), 6); //This is the same as above
}

void laserHarpSetScale(scale newScale, char octave) {
	char notes[MAX_BEAMS] = getScaleAtOctave(newScale, octave);
	unsigned char i = 0;
	for(i=0;i<MAX_BEAMS;i++) {
		beam_array[i].note = notes[i];
	}
}

void laserHarpStartStop(void){
	harp_enable ? laserHarpStop() : laserHarpStart();
}

void laserHarpStart(void){
	harp_enable = 1;
	hal_MotorDriver_SpinCW();
}

void laserHarpStop(void){
	harp_enable = 0;
	hal_MotorDriver_Standby();
}

/*
 * Function that will tell the laser when to fire
 */
void LaserFireCheck(uint8_t encoderIndex) {
	//@todo Use a more flexible method for configuring beams.
	switch(encoderIndex){
	case 44:
		current_beam = &beam_array[0];
		break;
	case 45:
		current_beam = &beam_array[1];
		break;
	case 46:
		current_beam = &beam_array[2];
		break;
	case 47:
		current_beam = &beam_array[3];
		break;
	case 0:
		current_beam = &beam_array[4];
		break;
	case 1:
		current_beam = &beam_array[5];
		break;
	case 2:
		current_beam = &beam_array[6];
		break;
	case 3:
		current_beam = &beam_array[7];
		break;
	case 4:
		current_beam = &beam_array[8];
		break;
	}
	if((encoderIndex < 5) || (43 < encoderIndex)) LaserOperate();
}

void LaserOperate(void) {
	P4OUT |= BIT3;				// Turn on the laser
	TA1CTL |= MC_1+TACLR;		// Starts the timer and makes the timer count up to TA1CCR0
	ADC12CTL0 |= ADC12SC;       // Start ADC sampling/conversion
}

static void disableLockout(beam_t * beam) {
	beam->lockout_flag = 0;
}

static void processBeam(beam_t * beam) {
	//Calculate "rolling" average
	beam->average_intensity = (0.90 * beam->average_intensity) + (100 * beam_broken_flag);

	if(beam->lockout_flag == 1) return;


	if(beam->average_intensity >= BEAM_THRESHOLD){
		if(beam->note_on_flag != 1){
			MIDI_SendNote(0,beam->note,127);
			beam->note_on_flag = 1;
			beam->lockout_flag = 1;
			Task_Schedule(disableLockout, beam, 100, 0);
		}
	}else{ //Beam is not broken
		if(beam->note_on_flag == 1){
			MIDI_SendNoteOff(0,beam->note);
			beam->note_on_flag = 0;
			beam->lockout_flag = 1;
			Task_Schedule(disableLockout, beam, 100, 0);
		}
	}
}

// Timer1_A3 Interrupt Vector (TAIV) handler
// Run at end of beam
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=TIMER1_A0_VECTOR
__interrupt void TIMER1_A0_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(TIMER1_A0_VECTOR))) TIMER1_A0_ISR (void)
#else
#error Compiler not supported!
#endif
{
	TA1CTL &= 0xFFE7; 		// Stop Timer @todo Fix hardcoded control.
	TA1CCTL0 &= ~(CCIFG); 	//Clear flag
	TA1CCTL1 &= ~(CCIFG);
	TA1CCTL2 &= ~(CCIFG);
	P4OUT &= ~(BIT3);

	processBeam(current_beam);
}

// ADC12 Interrupt Vector (ADC12IV) handler
// Process ADC sample to determine if beam is broken.
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = ADC12_VECTOR
__interrupt void ADC12_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(ADC12_VECTOR))) ADC12_ISR (void)
#else
#error Compiler not supported!
#endif
{
  switch(__even_in_range(ADC12IV,34))
  {
  case  0: break;                           // Vector  0:  No interrupt
  case  2: break;                           // Vector  2:  ADC overflow
  case  4: break;                           // Vector  4:  ADC timing overflow
  case  6:                                  // Vector  6:  ADC12IFG0
	  beam_broken_flag = ADC12MEM0 >= LIGHT_SENSOR_THRESHOLD;
  	  break;
  case  8: break;                           // Vector  8:  ADC12IFG1
  case 10: break;                           // Vector 10:  ADC12IFG2
  case 12: break;                           // Vector 12:  ADC12IFG3
  case 14: break;                           // Vector 14:  ADC12IFG4
  case 16: break;                           // Vector 16:  ADC12IFG5
  case 18: break;                           // Vector 18:  ADC12IFG6
  case 20: break;                           // Vector 20:  ADC12IFG7
  case 22: break;                           // Vector 22:  ADC12IFG8
  case 24: break;                           // Vector 24:  ADC12IFG9
  case 26: break;                           // Vector 26:  ADC12IFG10
  case 28: break;                           // Vector 28:  ADC12IFG11
  case 30: break;                           // Vector 30:  ADC12IFG12
  case 32: break;                           // Vector 32:  ADC12IFG13
  case 34: break;                           // Vector 34:  ADC12IFG14
  default: break;
  }
}

/** @endcode */
