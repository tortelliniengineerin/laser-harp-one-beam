/**
	@mainpage Tortellini Engineerin Laser Harp
	@section sec_1 Initial Goals
    @subsection intro Introduction
    A laser harp is a type of musical instrument that is similar to a harp. However, instead of using real strings that will play a note when plucked, physical strings are replaced by lasers. When a hand covers a laser, a note is played. Tortellini Engineerin developed a laser harp that used a single laser on the Texas Instruments MSP430F5529 microcontroller.

    @subsection motive Motivation
    The goal was to develop an embedded device that could play music through the MIDI (musical instrument digital interface) protocol. This device can be connected to a laptop and play music through a DAW (digital audio workstation). Along with embedded devices created by other teams in the class, a band could be formed and play music. A secondary goal was to have every instrument communicate to a central computer over Bluetooth so that each instrument could use a different synthesizer in a digital audio workstation.

    A laser harp is a relatively new type of musical instrument that can play music through a built-in speaker or through MIDI and visually looks impressive when paired with a fog machine or hazer.

    @section PaV Photos and Videos
    A selection of photos and videos from the laser harp can be seen below.

    [Instagram video demonstrating laser harp communication over BLE](https://www.instagram.com/p/BEcXJxZQRdr/)

 	[Instagram video demonstrating finished laser harp](https://www.instagram.com/p/BE92A25wRfy/)

	@image html 1_LaserHarp_Solidworks_Tri_300pxw.png "Figure 1 Original Solidworks Design Model (Laserharp off)"
	@n
	@image html 2_LaserHarp_Solidworks_Lasers_1_300pxw.png "Figure 2 Original Solidworks Design Model (Laserharp on)"
	@n
	@image html 3_IMG_1883_300pxw.jpg "Figure 3 MSP430F5529 microcontroller used for main control logic"
	@n
	@image html 4_IMG_1880_300pxw.jpg "Figure 4 Class 3b, 800mW blue laser"
	@n
	@image html 5_IMG_1879_300pxw.jpg "Figure 5 Built in Laser PWM control circuit"
	@n
	@image html 6_IMG_20160508_192422003_300pxw.jpg "Figure 6 Photodiode light sensor mounted to box plate"
	@n
	@image html 7_IMG_1885_300pxw.jpg "Figure 7 TB6612FNG Dual Motor Driver"
	@n
	@image html 8_IMG_1884_300pxw.jpg "Figure 8 Adafruit Bluefruit BLE module"
	@n
	@image html 9_IMG_1886_300pxw.jpg "Figure 9 Photodiode Signal amplifiaction and lowpass filtering circuit"
	@n
	@image html 10_IMG_1882_300pxw.jpg "Figure 10 Completed Hardware Circuitry PCB "
	@n
	@image html 11_IMG_1866_300pxw.jpg "Figure 11 48 CPR motorencoder attached to Mirror spinning motor. Red flag is used for initilization."
	@n
	@image html 12_IMG_1869_300pxw.jpg "Figure 12 Mirror attached to 5000rpm brushless motor"
	@n
	@image html 13_IMG_1875_300pxw.jpg "Figure 13 Hardware PCB fully connected to MSP430"
	@n
	@image html 14_IMG_1878_300pxw.jpg "Figure 14 Laser and Mirror Motor fully attached to protective steel housing"
	@n
	@image html 15_IMG_1877_300pxw.jpg "Figure 15 Completed Laser Harp (off)"
	@n
	@image html 16_IMG_20160503_182430477_300pxw.jpg "Figure 16 Completed Laser Harp (on with fog illuminating lasers)"
	@n

    @section sec_4 Operation and Use
    A single blue class B laser was used for the harp's strings. The laser flashed off and on at set intervals as the beam reflected off a rapidly spinning mirror. This created the illusion of several beams being present at the same time at different angles. There was a motor which was encoded to provide feedback about the mirror's position. A light sensor was placed at the base off the laser harp and triggered an interrupt on our microcontroller. The interrupt firing would cause a MIDI note for the corresponding beam to be played.

    The system required a 12V power supply to operate the laser and a 6V power supply to operate the motor. The MSP430 could be powered through a standard 5V USB power supply. When the microcontroller powered on, it would wait until a button was pressed to start spinning the motor and pulsing the laser. This function could then be halted by pressing the button again.

    The MSP430 originally used the UART (universal asynchronous receiver/transmitter) protocol to communicate with a computer. A Python script was developed that could read new bytes from the microcontroller and translate those into MIDI messages. These messages could then be read by audio software.

    To communicate over Bluetooth, Apple's Bluetooth Low Energy MIDI specification was followed. A MIDI BLE interface was developed that worked with the AdaFruit BlueFruit board to send MIDI messages wirelessly to a nearby device. It was determined to work by sending arbitrary MIDI messages. However, issues arose when trying to handle playback. Windows did not have much documentation to support Bluetooth Low-Energy and it was not possible to modify the existing UART Python script to support BLE.

    Through a laptop running Ubuntu, a Python script was developed that could read and parse MIDI messages sent over Bluetooth Low-Energy. For individuals who may not want to dual boot their laptops, an Android app was developed and published on Google Play that also allowed users to read and listen to MIDI over BLE.

    In the Laser Harp project, directives were used to quickly switch between sending MIDI over UART or BLE based so that we could test the harp in either Windows or Linux.

    @section sec_5 Results and Discussion
    The final demonstration of our laser harp was fairly successful. With the use of a fog machine, it was easy to see each laser and hover over each beam to play a corresponding note. MIDI was sent to a laptop and playback worked flawlessly. The entire band was able to play a rendition of "Row Row Row Your Boat" by sending MIDI messages to a laptop running Tortellini's MIDI receiver software. The laptop, running Ableton Live, could change the sound font being used on each MIDI channel and perform advanced controls over each instrument's volume and synthesizer properties.

    There were a small number of issues that did arise during the development and final demo of the project. The BlueFruit was preventing too many messages from being sent in a given period of time and a lot of data was being lost. This was preventing a lot of notes from being turned off and we'd continually have to send a Midi Panic message. The harp switched from using BLE to using UART and our responsiveness was much better.

    Although the fog machine was helpful in letting the user see the beams in front of them, it was still difficult to get some notes to play. As the light sensor was pointed straight up, the reflection of the beams on steep angles were hard to detect. The user would have to explicitly curl their hand so as to reflect the beam toward the light sensor.

    @section agile Use of Agile Methodology
    @subsection slack Helpfulness of Slack
    Through the development of this project, Slack was used as a standard communication platform for each team and across teams. For this purpose it worked very well. Internal discussions about the project were done in our private group and we could quickly share thoughts and upload different types of media and code. Agreeing to use Slack instead of one or several other platforms was a good decision that has made it much easier going forward to keep everybody informed about our progress as well as ping specific team members.

    The inter-team communication also was benefited from Slack. Many people asked questions in the general channel and could receive an answer from anybody. If there was an ongoing problem, like when the Tortellini Player app was crashing on certain phones, everybody was able to stay informed about it rather than sending an isolated email and receiving the same reply. It was easy to see the original bug report and know when the app was updated.

    Inter-team communication also made it easy for members on different teams to directly message others. If there was a specific question that didn't require a group response, a chat could occur between two members privately. We sent direct messages to several classmates as well as the professor throughout the development of the laser harp.

    @subsection trello Helpfulness of Trello
    Trello was used as a way to keep track of tasks through each sprint. Having a centralized task list was useful, and with group projects it gave a clear idea of which team members were doing what and what tasks still needed to be completed. To better observe how many points we were completing through Scrum, the Chrome extension Scrum for Trello was installed.

    Toward the end the Trello board became stale and stopped receiving updates, but overall it was a good tool for productivity that is being used for many other teams at the university and using it for this class was helpful.

	@subsection code Code

	All header files have been documented using doxygen and placed into their respective groups. All respective source files (.c files) have been grouped and commmented throughly.


 @see hal_MotorDriver.h
 @see hal_MotorEncoder.h
 @see harp.h
 @see main.c
 @see MotorEncoder.h
 @see scales.h
 @see system.h

*/




/**
 * @ingroup Laserharp
 * @file system.h
 *
 * @author Michael Paule
 * @author Chris Frederickson
 * @{
 */

/*
 * system.h
 *
 *  Created on: Apr 3, 2016
 *      Author: Michael Paule
 *
 *MSP430 pins used
 *
 *  P2.1 is the button on the launchpad that controls the on and off of the laser harp
 *
 *  //Motor driver uses these guys
 *  PWMA - P6.6
 *  AIN2 - P6.1
 *  AIN1 - P6.2
 *  STBY - P6.3
 *  VMOT - 6V on the power supply
 *
 *	//motor encoder- line the red tapes up
 *	Channel A on P1.4 (looking for first pos edge) , then from then on it just switchs
 *	Channel B on P1.5 (looking for first pos edge too)
 *
 *
 *	//UART pins- to the Bluefruit
 *	P3.4 RX
 *	P3.3 TX
 *
 *	//GPIO for laser control
 *	P4.3 - TA1.1  TA1 CCR1 capture - PWM that is on for 60 microseconds, then off, with a frequency of 80 Hz
 *
 *	//ADC12 Vin Light sensor outpu
 *	P6.0
 *
 *Bluefruit controller circuit
 *	RX -connected to P3.3 TX
 *	TX -connected to P3.4 RX
 *	CTS grounded
 *	Vin 3.3V
 *	MOD,DFU, and RTS left floating
 *
 *PhotoDiode Circuit
 *	Simple low pass filter and op amp circuit were used to cut out noise and amplify the signal
 *
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#define FCPU 25000000
#define PERIPHERAL_CLOCK FCPU

#define USE_UART0
#define USE_UART1
#define UART_BAUD 9600
#define UART_CHANNEL 1
#define BLUEFRUIT_CHANNEL 0

#define UART_TX_BUFFER_LENGTH 512
#define UART_RX_BUFFER_LENGTH 512
#define UART_MAX_CHARS_PER_CALL 5

#include <stdint.h>

// include the library header
#include "library.h"
// include list of modules used
#include "task.h"
#include "timing.h"
#include "list.h"
#include "buffer.h"
#include "buffer_printf.h"
#include "charReceiverList.h"
#include "itemBuffer.h"
#include "uart.h"
#include "bluefruit.h"
#include "midi_ble.h"
#include "midi.h"

#include "hal_MotorDriver.h"
#include "hal_MotorEncoder.h"
#include "MotorEncoder.h"
#include "harp.h"

/** @} */
#endif	/* SYSTEM_H */
