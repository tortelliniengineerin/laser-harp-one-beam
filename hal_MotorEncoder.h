#ifndef HAL_MOTORENCODER_H_
#define HAL_MOTORENCODER_H_

/**
 * @ingroup hal_Laserharp
 * @file hal_MotorEncoder.h
 *
 *	Created on: Apr 4, 2016
 *      Author: Michael Paule
 *
 *  This HAL layer handles the reading of the positive and negative edges of a quadrature motor encoder
 *  using interupts. The ISR for port 1 is included in this HAL and it calls the function void EncoderProcessEdge(void).
 *
 * @see MotorEncoder.c for how edges are processed
 * @author Michael Paule
 * @{
 */

/*
 * hal_MotorEncoder.h
 *
 *  Created on: Apr 4, 2016
 *      Author: Michael Paule
 *
 *  This HAL layer handles the reading of the positive and negative edges of a quadrature motor encoder
 *  using interupts. The ISR for port 1 is included in this HAL and it calls the function void EncoderProcessEdge(void).
 */

#include "system.h"

/** @brief Initilize pins P1.4 and P1.5 for interupts, channels A and B of the encoder respectively
 *
 *
 */
void hal_MotorEncoder_Init(void);


/** @} */
#endif /* HAL_MOTORENCODER_H_ */
