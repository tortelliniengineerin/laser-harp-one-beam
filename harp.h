/**
 * @author Michael Paule
 * @author Chris Frederickson
 * @author Nick Felker
 * @author Kyle Siebens
 *
 * Created on: Apr 26, 2016
 *
 * This module contains all of the main operations of the Laserharp. These operations include encoder reading, light sensing, and MIDI messaging.
 * The harp file specifically contains the main control system for the laser harp.
 *
 *
 * @defgroup Laserharp Laserharp Module
 * @file harp.h
 * @ingroup Laserharp
 *
 * @author Michael Paule
 * @author Chris Frederickson
 *
 * Created on: Apr 26, 2016
 *      Author: Chris and Mike
 *
 * The harp module contains all laser harp specific logic. The harp module uses the motor encoder as an input to trigger all beam execution.
 * At each encoder tick, the harp module determines if the beam should fire. During the laser operation, the ADC takes a sample and updates
 * beam_broken_flag. After a set time, the module determines whether the beam was blocked and sends the corresponding MIDI messages.
 *
 * All beam related configuration is stored in the beam_t struct. These structs are initilized in order to control the corresponding MIDI notes.
 *
 * @{
 */

/* harp.h
 *
 * Created on: Apr 26, 2016
 *      Author: Chris
 */

#ifndef HARP_H_
#define HARP_H_

#include <stdint.h>

#ifndef PULSE_LENGTH
#define PULSE_LENGTH 20			// In microseconds
#endif

#ifndef LIGHT_SENSOR_THRESHOLD
#define LIGHT_SENSOR_THRESHOLD 1000
#endif

#ifndef TA1_MAX_VALUE
#define TA1_MAX_VALUE UINT16_MAX
#endif

#ifndef LASER_ON_CLOCKS
#define LASER_ON_CLOCKS FCPU/16667 // 16667 Hz = 60 microseconds
#endif

#ifndef MAX_BEAMS
#define MAX_BEAMS 9
#endif

#ifndef MAX_POSITION
#define MAX_POSITION 3599 // 360 degrees in 10th of a degree increments
#endif

/**
 * @brief Beam Data Structure
 *
 */
typedef struct {
	uint8_t is_valid; 			/** Is the beam initilized? */
	uint16_t position;			/** Where should be beam be located? Currently unused. */
	uint8_t note;				/** What note shoud the beam play? */
	uint32_t average_intensity;	/** Used for low pass filtering the beam broken state */
	uint8_t note_on_flag;		/** Has the MIDI send note message been sent? */
	uint8_t lockout_flag;		/** Should the beam be skipped to prevent double notes? */
} beam_t;

/**
 * @brief Initilizes the laser harp
 *
 *
 */
void laserHarpInit(void);

/**
 * @brief Toggles the enable state of the laser harp
 *
 *
 */
void laserHarpStartStop(void);

/**
 * @brief Enables the laser harp
 *
 *
 */
void laserHarpStart(void);

/**
 * @brief Disables the laser harp
 *
 *
 */
void laserHarpStop(void);

/**
 * @brief Function that will tell the laser when to fire
 *
 * @param encoderIndex - the current encoder position
 */
void LaserFireCheck(uint8_t encoderIndex);

/**
 * @brief Function that operates the laser
 *
 *
 */
void LaserOperate(void);

/**
 * @brief Disable the lockout flag for a beam
 *
 * The lockout flag is used to prevent a note from being sent twice in quick succession.
 *
 * @param beam - a pointer to the particular beam
 */
static void disableLockout(beam_t * beam);

/**
 * @brief Process the note sending for beam
 *
 * Process beam is run to determine which midi actions should be performed for a given beam.
 * Process beam assumes that beam_broken_flag was updated before execution.
 *
 * @param beam - a pointer to the particular beam
 */
static void processBeam(beam_t * beam);

/** @} */
#endif /* HARP_H_ */
