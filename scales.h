
/**
 * @ingroup Laserharp
 * @file scales.h
 *
 * @author Nick Felker
 * @author Michael Paule
 *
 * Created on: Apr 29, 2016
 *      Author: Nick Felker
 *	This file allows the laserharp beams to be set to specfic notes in various scales that can be easily swapped in an out.
 *
 * @{
 */


/*
 * scales.h
 *
 *  Created on: Apr 29, 2016
 *      Author: Nick Felker
 *  This file allows the laserharp beams to be set to specfic notes in various scales that can be easily swapped in an out.
 */

#ifndef SCALES_H_
#define SCALES_H_

#define _NOTE_C  0
#define _NOTE_Cs 1
#define _NOTE_Db 1
#define _NOTE_D  2
#define _NOTE_Ds 3
#define _NOTE_Eb 4
#define _NOTE_E  4
#define _NOTE_F  5
#define _NOTE_Fs 6
#define _NOTE_Gb 6
#define _NOTE_G  7
#define _NOTE_Gs 8
#define _NOTE_Ab 9
#define _NOTE_A  9
#define _NOTE_As 10
#define _NOTE_Bb 10
#define _NOTE_B  11

#ifndef MAX_BEAMS
#define MAX_BEAMS 9 //Just in case we didn't get this set
#endif

typedef struct {
	char scale_size;
	char notes[12]; //We can't really have more than 12 notes here
} scale;


#define C_MAJOR  1
#define G_MAJOR 2
#define D_MAJOR 3
#define A_MAJOR  4
#define E_MAJOR  5
#define B_MAJOR  6
#define Gb_MAJOR 7
#define Db_MAJOR 8
#define Ab_MAJOR 9
#define Eb_MAJOR 10
#define Bb_MAJOR 11
#define F_MAJOR 12
#define BLUES_SCALE 13
#define BEBOP_SCALE 14
#define HIRAJOSHI_SCALE 15
#define YO_SCALE 16
#define SEXY_CHORD 17

/**
 * @brief This function allows us to pick a particular note at a particular octave.
 * @par  Use _NOTE_** providing either a regular note or an accidental by using `s` to denote sharp and `b` to denote flat
 *
 * @par Valid octaves exist between 0 and 10, although the 10th octave cuts off at G
 */
char getNoteAtOctave(char note, char octave);

/**
 * @brief Pass a scale struct and an octave and it will generate a char array of
 *   appropriate notes of size MAX_BEAMS
 */
char* getScaleAtOctave(scale thisScale, char octave);

/**
 * @brief Pass in the name of the default scale you want and it will be returned. If the name is invalid,
 *   you will receive a C-Scale
 */
scale getScale(char scale);

/**
 * @brief If you want to change the mode, the starting note, pass in the scale you want
 *   and the note that you want to start at
 */
scale shiftScale(scale origScale, char offset);

/** @} */
#endif /* SCALES_H_ */
