#include "system.h"
/**
 * @file hal_MotorDriver.c
 * @ingroup hal_Laserharp
 * @author Michael Paule
 * @code
 */
void hal_MotorDriver_Init(void){
	P6DIR |= AIN1 + AIN2 + STBY;
	P6DIR |= PWMA;
	hal_MotorDriver_Standby();
}

void hal_MotorDriver_SpinCW(void){ //function code is HLHH - IN1,IN2,PWM,STBY
	P6OUT |= AIN1 + STBY + PWMA;
	P6OUT &= ~AIN2;
}

void hal_MotorDriver_SpinCCW(void){//function code is LHHH - IN1,IN2,PWM,STBY
	P6OUT |= AIN2 + STBY + PWMA;
	P6OUT &= ~AIN1;
}

void hal_MotorDriver_Stop(void){//function code is LLHH - IN1,IN2,PWM,STBY
	P6OUT |= STBY + PWMA;
	P6OUT &= ~(AIN1+AIN2) ;
}

void hal_MotorDriver_Standby(void){
	P6OUT &= ~(STBY+AIN1+AIN2+PWMA);
}

/** @endcode */
